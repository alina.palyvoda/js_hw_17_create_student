/**
 * Created by Алина on 15.01.2022.
 */

function createNewStudent() {
    let studentName = prompt("Введіть ім'я студента");
    while (!studentName) {
        studentName = prompt("Введіть ім'я студента");
    }
    let studentLastName = prompt("Введіть прізвище студента");
    while (!studentLastName) {
        studentLastName = prompt("Введіть прізвище студента");
    }
    const studentTabel = {};
    while (true) {
        let subject = prompt("Введіть назву дисципліни");
        if (!subject) break;
        let gradePoint = +prompt("Введіть оцінку з дисципліни");
        if (!gradePoint) break;
        studentTabel[subject] = gradePoint
    }
    return {
        studentName,
        studentLastName,
        studentTabel,
    }
}

let student = createNewStudent();
let studentGradePoints = Object.values(student.studentTabel);

function showMessage() {
    function getResult() {
        let result = true
        for (let key in student.studentTabel) {
            if (student.studentTabel[key] <= 4) {
                alert("Студента направлено на пересдачу");
                result = false
                break
            }
        }
        return result
    }

    let result = getResult();

    function getAverage() {
        let sum = 0;
        let len = studentGradePoints.length;
        for (let i = 0; i < len; i++) {
            sum = studentGradePoints[i] + sum
        }
        return sum / len;
    }

    let average = getAverage();
    if (result === true && average > 7) {
        alert("Студенту призначено стипендію")
    } else if (result === true && average <= 7) {
        alert("Студента переведено на наступний курс")
    }
}

let message = showMessage()
